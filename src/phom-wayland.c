/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phom-wayland"

#include "config.h"
#include "phom-enums.h"
#include "phom-wayland.h"

#include <gdk/gdkwayland.h>


enum {
  PROP_0,
  PROP_HAS_OUTPUT,
  PROP_HAS_POINTER,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];


struct _PhomWayland {
  GObject parent;

  struct wl_seat *seat;
  struct wl_display *display;
  struct wl_registry *registry;
  struct zwlr_virtual_pointer_manager_v1 *pointer_manager;
  struct zwlr_virtual_pointer_v1 *pointer;
  GHashTable *wl_outputs;
  struct wl_output *wl_output;
};

G_DEFINE_TYPE (PhomWayland, phom_wayland, G_TYPE_OBJECT)


static void
phom_wayland_enable_pointer (PhomWayland *self)
{
  if (self->pointer != NULL)
    return;

  g_debug ("Enabling virtual pointer");
  self->pointer = zwlr_virtual_pointer_manager_v1_create_virtual_pointer_with_output(
      self->pointer_manager, self->seat, self->wl_output);


  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_HAS_POINTER]);
}


static void
phom_wayland_disable_pointer (PhomWayland *self)
{
  if (self->pointer == NULL)
    return;

  g_debug ("Disabling virtual pointer");
  g_clear_pointer (&self->pointer, zwlr_virtual_pointer_v1_destroy);
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_HAS_POINTER]);
}


static void
phom_wayland_get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  PhomWayland *self = PHOM_WAYLAND (object);

  switch (property_id) {
  case PROP_HAS_OUTPUT:
    g_value_set_boolean (value, !!self->wl_output);
    break;
  case PROP_HAS_POINTER:
    g_value_set_boolean (value, !!self->pointer);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
output_handle_geometry (void             *data,
                        struct wl_output *wl_output,
                        int               x,
                        int               y,
                        int               physical_width,
                        int               physical_height,
                        int               subpixel,
                        const char       *make,
                        const char       *model,
                        int32_t           transform)
{
  g_debug ("handle geometry output %p, position %d %d, size %dx%d, subpixel layout %d, vendor %s, "
           "product %s, transform %d",
           data, x, y, physical_width, physical_height, subpixel, make, model, transform);
}


static void
output_handle_done (void             *data,
                    struct wl_output *wl_output)
{
}


static void
output_handle_scale (void             *data,
                     struct wl_output *wl_output,
                     int32_t           scale)
{
}


static void
output_handle_mode (void             *data,
                    struct wl_output *wl_output,
                    uint32_t          flags,
                    int               width,
                    int               height,
                    int               refresh)
{
  PhomWayland *self = PHOM_WAYLAND (data);
  gboolean had_output;

  g_return_if_fail (PHOM_IS_WAYLAND (self));
  had_output = !!self->wl_output;

  /* TODO: Use xdg-output to select by connector */
  if (width == 720 && height == 1440)
    return;

  if (width == 360 && height == 720)
    return;

  self->wl_output = wl_output;
  g_debug ("Using %p (%dx%d) as output", wl_output, width, height);

  phom_wayland_enable_pointer (self);

  if (had_output == FALSE)
    g_object_notify_by_pspec (G_OBJECT (self), props[PROP_HAS_OUTPUT]);
}


static const struct wl_output_listener output_listener =
{
  output_handle_geometry,
  output_handle_mode,
  output_handle_done,
  output_handle_scale,
};


static void
registry_handle_global (void *data,
                        struct wl_registry *registry,
                        uint32_t name,
                        const char *interface,
                        uint32_t version)
{
  PhomWayland *self = data;
  struct wl_output *output;

  if (!strcmp (interface, wl_seat_interface.name)) {
    self->seat = wl_registry_bind(
      registry,
      name,
      &wl_seat_interface,
      1);
  } else if (!strcmp (interface, "wl_output")) {
    output = wl_registry_bind (
      registry,
      name,
      &wl_output_interface, 2);
    g_debug ("Got new output %p", output);
    g_hash_table_insert (self->wl_outputs, GINT_TO_POINTER (name), output);
    wl_output_add_listener (output, &output_listener, self);
  } else if (!strcmp (interface, zwlr_virtual_pointer_manager_v1_interface.name)) {
    self->pointer_manager = wl_registry_bind(
      registry,
      name,
      &zwlr_virtual_pointer_manager_v1_interface,
      2);
  }
}


static void
registry_handle_global_remove (void *data,
                               struct wl_registry *registry,
                               uint32_t name)
{
  PhomWayland *self = PHOM_WAYLAND (data);
  struct wl_output *wl_output;

  wl_output = g_hash_table_lookup (self->wl_outputs, GINT_TO_POINTER (name));
  if (wl_output) {
    g_debug ("Output %d removed", name);
    g_hash_table_remove (self->wl_outputs, GINT_TO_POINTER (name));
    wl_output_destroy (wl_output);

    if (wl_output == self->wl_output) {
      self->wl_output = NULL;
      g_object_notify_by_pspec (G_OBJECT (self), props[PROP_HAS_OUTPUT]);

      phom_wayland_disable_pointer (self);
    }
  } else {
    g_warning ("Global %d removed but not handled", name);
  }
}


static const struct wl_registry_listener registry_listener = {
  registry_handle_global,
  registry_handle_global_remove
};

static void
phom_wayland_constructed (GObject *object)
{
  PhomWayland *self = PHOM_WAYLAND (object);
  GdkDisplay *gdk_display;
  int num_outputs;

  G_OBJECT_CLASS (phom_wayland_parent_class)->constructed (object);

  gdk_display = gdk_display_get_default ();
  self->display = gdk_wayland_display_get_wl_display (gdk_display);

  if (self->display == NULL) {
      g_error ("Failed to get display: %m\n");
  }

  self->registry = wl_display_get_registry (self->display);
  wl_registry_add_listener (self->registry, &registry_listener, self);

  /* Wait until we have been notified about the wayland globals we require */
  wl_display_roundtrip(self->display);
  num_outputs = g_hash_table_size(self->wl_outputs);
  if (!num_outputs || !self->pointer_manager) {
    g_error ("Could not find needed globals\n"
             "outputs: %d, "
             "virtual_pointer_manager: %p, "
             "wl_seat: %p"
             "\n",
             num_outputs, self->pointer_manager, self->seat);
  }
}


static void
phom_wayland_dispose (GObject *object)
{
  PhomWayland *self = PHOM_WAYLAND (object);

  phom_wayland_disable_pointer (self);
  g_clear_pointer (&self->pointer_manager, zwlr_virtual_pointer_manager_v1_destroy);
  g_clear_pointer (&self->wl_outputs, g_hash_table_destroy);

  G_OBJECT_CLASS (phom_wayland_parent_class)->dispose (object);
}


static void
phom_wayland_class_init (PhomWaylandClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = phom_wayland_get_property;
  object_class->constructed = phom_wayland_constructed;
  object_class->dispose = phom_wayland_dispose;

  /**
   * PhomWayland:has-output
   *
   * Whether there's an output for the pointer to use.
   */
  props[PROP_HAS_OUTPUT] =
    g_param_spec_boolean ("has-output", "", "",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * PhomWayland:has-pointer
   *
   * Whether there's a virtual pointer.
   */
  props[PROP_HAS_POINTER] =
    g_param_spec_boolean ("has-pointer", "", "",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);


  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
phom_wayland_init (PhomWayland *self)
{
  self->wl_outputs = g_hash_table_new (g_direct_hash, g_direct_equal);
}


PhomWayland *
phom_wayland_get_default (void)
{
  static PhomWayland *instance;

  if (instance == NULL) {
    instance = g_object_new (PHOM_TYPE_WAYLAND, NULL);
    g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *)&instance);
  }
  return instance;
}

void
phom_wayland_button_press (PhomWayland *self, PhomWaylandBtn button, gboolean press)
{
  guint64 timestamp = g_get_monotonic_time () / 1000;

  g_return_if_fail (PHOM_IS_WAYLAND (self));
  g_return_if_fail (self->pointer);

  zwlr_virtual_pointer_v1_button(self->pointer, timestamp, button, press);
}


void
phom_wayland_motion_absolute (PhomWayland *self, int x, int y, int dx, int dy)
{
  guint64 timestamp = g_get_monotonic_time () / 1000;

  g_return_if_fail (PHOM_IS_WAYLAND (self));
  g_return_if_fail (self->pointer);

  zwlr_virtual_pointer_v1_motion_absolute(self->pointer, timestamp, x, y, dx, dy);
}
