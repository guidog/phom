/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#pragma once

#include <handy.h>

G_BEGIN_DECLS

#define PHOM_TYPE_WINDOW (phom_window_get_type())

G_DECLARE_FINAL_TYPE (PhomWindow, phom_window, PHOM, WINDOW, HdyApplicationWindow);

PhomWindow *phom_window_new (GtkApplication *app);

G_END_DECLS
