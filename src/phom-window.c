/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phom-window"

#include "config.h"
#include "phom-window.h"
#include "phom-wayland.h"

#define A11Y_KEY_OSK "screen-keyboard-enabled"

enum {
  PROP_0,
  PROP_MOUSE_ENABLED,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];

struct _PhomWindow
{
  HdyApplicationWindow parent;

  GtkWidget      *evbox_move;
  GtkWidget      *btn_osk;
  GtkWidget      *img_mouse;

  PhomWayland   *wayland;

  gboolean       mouse_enabled;
};

G_DEFINE_TYPE(PhomWindow, phom_window, HDY_TYPE_APPLICATION_WINDOW);


static void
phom_window_set_mouse_enabled (PhomWindow *self, gboolean enabled)
{
  GtkStyleContext *context;

  if (enabled == self->mouse_enabled)
    return;

  self->mouse_enabled = enabled;
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_MOUSE_ENABLED]);

  context = gtk_widget_get_style_context (self->img_mouse);

  if (enabled)
    gtk_style_context_remove_class (context, "disabled");
  else
    gtk_style_context_add_class (context, "disabled");
}


static void
phom_window_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PhomWindow *self = PHOM_WINDOW (object);

  switch (property_id) {
  case PROP_MOUSE_ENABLED:
    phom_window_set_mouse_enabled (self, g_value_get_boolean (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phom_window_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PhomWindow *self = PHOM_WINDOW (object);

  switch (property_id) {
  case PROP_MOUSE_ENABLED:
    g_value_set_boolean (value, self->mouse_enabled);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
on_left_button_pressed (PhomWindow *self)
{
  phom_wayland_button_press (self->wayland, PHOM_WAYLAND_BTN_LEFT, TRUE);
}


static void
on_left_button_released (PhomWindow *self)
{
  phom_wayland_button_press (self->wayland, PHOM_WAYLAND_BTN_LEFT, FALSE);
}


static void
on_right_button_pressed (PhomWindow *self)
{
  phom_wayland_button_press (self->wayland, PHOM_WAYLAND_BTN_RIGHT, TRUE);
}


static void
on_right_button_released (PhomWindow *self)
{
  phom_wayland_button_press (self->wayland, PHOM_WAYLAND_BTN_RIGHT, FALSE);
}


static gboolean
on_touch_event (PhomWindow *self, GdkEventTouch *event, GtkEventBox evbox)
{
  int x, y;
  g_autofree gchar *t = g_enum_to_string (GDK_TYPE_EVENT_TYPE, event->type);

  g_return_val_if_fail (PHOM_IS_WINDOW (self), FALSE);

  g_debug ("x: %f, y: %f, type: %s", event->x, event->y, t);
  x = event->x;
  y = event->y;

  if (event->type == GDK_TOUCH_BEGIN || event->type == GDK_TOUCH_UPDATE) {
    int width = gtk_widget_get_allocated_width (self->evbox_move);
    int height = gtk_widget_get_allocated_height (self->evbox_move);

    phom_wayland_motion_absolute (self->wayland, x, y, width, height);
  }

  return FALSE;
}


static void
phom_window_constructed (GObject *object)
{
  PhomWindow *self = PHOM_WINDOW (object);
  g_autoptr (GSettings) a11y_settings = NULL;

  gtk_widget_add_events (self->evbox_move, GDK_TOUCH_MASK);
  g_signal_connect_swapped (self->evbox_move,
                            "touch-event",
                            G_CALLBACK (on_touch_event),
                            self);
  a11y_settings = g_settings_new ("org.gnome.desktop.a11y.applications");

  g_settings_bind (a11y_settings, A11Y_KEY_OSK, self->btn_osk, "active",
                   G_SETTINGS_BIND_DEFAULT);

  G_OBJECT_CLASS (phom_window_parent_class)->constructed (object);
}


static void
phom_window_class_init (PhomWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = phom_window_get_property;
  object_class->set_property = phom_window_set_property;
  object_class->constructed = phom_window_constructed;

  /**
   * PhomWindow:mouse-enabled
   *
   * Whether the virtual mouse is currently enabled. It can be disabled e.g. due to
   * a missing second output.
   */
  props[PROP_MOUSE_ENABLED] =
    g_param_spec_boolean ("mouse-enabled", "", "",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/sigxcpu/phom/ui/phom-window.ui");

  gtk_widget_class_bind_template_child (widget_class, PhomWindow, evbox_move);
  gtk_widget_class_bind_template_child (widget_class, PhomWindow, btn_osk);
  gtk_widget_class_bind_template_child (widget_class, PhomWindow, img_mouse);

  gtk_widget_class_bind_template_callback (widget_class, on_left_button_pressed);
  gtk_widget_class_bind_template_callback (widget_class, on_left_button_released);
  gtk_widget_class_bind_template_callback (widget_class, on_right_button_pressed);
  gtk_widget_class_bind_template_callback (widget_class, on_right_button_released);
}


static void
phom_window_init (PhomWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->wayland = phom_wayland_get_default ();

  g_object_bind_property (self->wayland,
                          "has-pointer",
                          self,
                          "mouse-enabled",
                          G_BINDING_SYNC_CREATE);
}


PhomWindow *
phom_window_new (GtkApplication *app)
{
  return g_object_new (PHOM_TYPE_WINDOW, "application", app, NULL);
}
