/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */


#include "phom-window.h"
#include "config.h"

#include <glib/gi18n.h>

#include <gtk/gtk.h>
#include <handy.h>


static void
show_about (GSimpleAction *action,
            GVariant      *state,
            gpointer       user_data)
{
  GtkApplication *app = GTK_APPLICATION (user_data);
  GtkWindow *window = gtk_application_get_active_window (app);
  const gchar *authors[] = {"Guido Günther", NULL};

  gtk_show_about_dialog (window,
                         "authors", authors,
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "logo-icon-name", "input-mouse-symbolic",
                         "program-name", "Virtual Mouse",
                         "title", _("About Virtual Mouse"),
                         "translator-credits", _("translator-credits"),
                         "version", PHOM_VERSION,
                         "website", "https://gitlab.gnome.org/guidog/phom",
                         NULL);
}


static void
css_setup (void)
{
  GtkCssProvider *provider;
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;

  provider = gtk_css_provider_new ();
  file = g_file_new_for_uri ("resource:///org/sigxcpu/phom/style.css");

  if (!gtk_css_provider_load_from_file (provider, file, &error)) {
    g_warning ("Failed to load CSS file: %s", error->message);
    return;
  }
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (provider), 600);
}


static void
on_activate (GtkApplication* app,
             gpointer        user_data)
{
  PhomWindow *window;

  window = phom_window_new (app);
  gtk_widget_show_all (GTK_WIDGET (window));
}


static void
on_startup (GtkApplication* app,
            gpointer        user_data)
{
  hdy_init ();
  css_setup ();

  gtk_icon_theme_add_resource_path (gtk_icon_theme_get_default (),
                                    "/org/sigxcpu/phom/icons");
}


int
main (int   argc,
      char *argv[])
{
  static GActionEntry app_entries[] = {
    { "about", show_about, NULL, NULL, NULL },
  };
  g_autoptr (GtkApplication) app = NULL;

  /* Simplify running nested */
  gdk_set_allowed_backends ("wayland");

  textdomain (GETTEXT_PACKAGE);
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

  g_set_prgname(PHOM_APP_ID);

  app = gtk_application_new (PHOM_APP_ID, G_APPLICATION_FLAGS_NONE);
  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
  g_signal_connect (app, "startup", G_CALLBACK (on_startup), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
