/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "wlr-virtual-pointer-unstable-v1-client-protocol.h"

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum {
  PHOM_WAYLAND_BTN_LEFT  = 0x110,
  PHOM_WAYLAND_BTN_RIGHT = 0x111,
} PhomWaylandBtn;

#define PHOM_TYPE_WAYLAND phom_wayland_get_type()

G_DECLARE_FINAL_TYPE (PhomWayland, phom_wayland, PHOM, WAYLAND, GObject)

PhomWayland  *phom_wayland_get_default (void);
void           phom_wayland_button_press (PhomWayland *self, PhomWaylandBtn btn, gboolean press);
void           phom_wayland_motion_absolute (PhomWayland *self, int x, int y, int dx, int dy);

G_END_DECLS
