# Phom

Virtual mouse for phosh

## License

phom is licensed under the GPLv3+.

## Getting the source

```sh
    git clone https://gitlab.gnome.org/guidog/phom
    cd phom
```

The master branch has the current development version.

## Dependencies
On a Debian based system run

```sh
    sudo apt-get -y install build-essential
    sudo apt-get -y build-dep .
```

For an explicit list of dependencies check the `Build-Depends` entry in the
[debian/control][] file.

## Building

We use the meson (and thereby Ninja) build system for phom.  The quickest
way to get going is to do the following:

    meson . _build
    ninja -C _build
    ninja -C _build install

## Running
### Running from the source tree
When running from the source tree use:

    _build/src/phom

# Getting in Touch
* Issue tracker: https://gitlab.gnome.org/guidog/phom
* Matrix: https://im.puri.sm/#/room/#phosh:talk.puri.sm

For details see the [developer documentation](https://developer.puri.sm/Contact.html).

[gitlab-ci.yml]: https://gitlab.gnome.org/guidog/phom/blob/main/.gitlab-ci.yml
[debian/control]: https://gitlab.gnome.org/guidog/phom/blob/main/debian/control
